This is a set of patches for Linux 6.0 to get it running on Samsung Exynos7870.

* patches/

Patches with hardware support, candidates for upstreaming.

* debug-patches/

Mostly adding debug prints here and there, should not be applied on "production" kernels.

* aakt/

Paravirtualized ramoops console.
